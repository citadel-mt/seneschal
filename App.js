const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const { api } = require('./routes/index');

const { port } = require('./config');
const app = express();

app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(morgan(':method :url; status: :status; :response-time ms - response: :res[content-length] bytes'));
app.use(cors({
   methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS', 'PROPFIND', 'MOVE']
}));

app.use('/', api);

app.use((req, res) => {
   res.status(404);
   res.json({
      error_code: 404,
      message: "Page is not found",
   });
   res.end();
});

app.use((err, req, res, next) => {
   console.log({
      error_code: err.status,
      error_message: err.message,
   });
   res.status(err.status);
   res.json({
      error_code: err.status,
      error_message: err.message,
   });
   res.end();
});

app.listen(port, () => {
   console.log(`Server listen on port ${port}`);
});
