const express = require('express');
const path = require('path');
/*const sendSeekable = require('send-seekable');
const getRawBody = require('raw-body');
const { getFileTree, deleteFile, createFile, readFile, createDir } = require('../utils/files');
const { rootDir } = require('../config');*/

const router = new express.Router();

router.get('/main.js', async (req, res) => {
    res.sendFile(path.join(path.dirname(require.main.filename), "src/files/dist/main.js"));
});

router.get('*', async (req, res) => {
    res.sendFile(path.join(path.dirname(require.main.filename), "src/files/static/index.html"));
});

/*router.use(sendSeekable);
router.use(function (req, res, next) {
    getRawBody(req, {
        length: req.headers['content-length']
    }, function (err, data) {
        if (err) return next(err);
        req.body = data;
        next()
    })
});

router.get('/list/all', async (req, res, next) => {
    try {
        const list = await getFileTree(rootDir);
        res.status(200);
        res.json({
            count: list.length,
            data: list
        });
    } catch (err) {
        next(err);
    }
});

router.get('/:path', async (req, res, next) => {
    try {
        const content = await readFile(rootDir, req.params.path);
        res.status(200);
        res.sendSeekable(content);
    } catch (err) {
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    try {
        await createFile(rootDir, req.headers.filename, req.body);
        res.status(200);
        res.json({
            data: "OK"
        })
    } catch (err) {
        next(err);
    }
});

router.post('/dir/:path', async (req, res, next) => {
    try {
        await createDir(rootDir, req.params.path);
        res.status(200);
        res.json({
            data: "OK"
        });
    } catch (err) {
        next(err);
    }
});

router.delete('/:path', async (req, res, next) => {
    try {
        await deleteFile(rootDir, req.params.path);
        res.status(200);
        res.json({
            data: req.params.path
        });
    } catch (err) {
        next(err);
    }
});*/

module.exports = { files: router };
