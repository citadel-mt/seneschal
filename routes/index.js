const express = require('express');
const webdav = require('webdav-server').v2;

const api = new express.Router();

const { webDavPath } = require('../config');
const { webdavMiddleware } = require('./webdavMiddleware');
const { files } = require('./files');
const { info } = require('./info');
const { users } = require('./users');
const { webDavServer } = require('./webdav');

api.use('/info', info);
api.use('/users', users);
api.use('/files', files);
api.use(webdavMiddleware(), webdav.extensions.express(webDavPath, webDavServer));

module.exports = { api };
