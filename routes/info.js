const express = require('express');
const pjson = require('../package.json');
const { serverName } = require('../config');

const router = new express.Router();

router.get('/', async (req, res) => {
    res.status(200);
    res.send(`${pjson.identifier}(server=${pjson.name},name=${serverName},version=${pjson.version})`);
});

module.exports = { info: router };
