const express = require('express');
const path = require('path');
const md5 = require('js-md5');
const base64 = require('base-64');

const { NoSuchUserException, WrongPasswordException } = require('../utils/errors');
const { addToken, removeToken } = require('../utils/tokens');
const { users } = require(path.join(path.dirname(require.main.filename), "users.js"));

const router = new express.Router();

router.post('/login', async (req, res, next) => {
    console.log(req.body.username);
    console.log(req.body.password);
    const user = users.find((u) => u.login === req.body.username);
    if (user === undefined) {
        next(new NoSuchUserException(req.body.username));
    } else {
        if (md5(user.password) === req.body.password) {
            const accessToken = md5(user.login + user.password + new Date().toString());
            const webDavToken = base64.encode(`${user.login}:${user.password}`);
            let token;
            if (req.body.needToRemember) {
                addToken(accessToken, webDavToken);
                token = accessToken;
            } else {
                token = webDavToken;
            }
            res.status(200);
            res.json({
                username: user.login,
                access_token: token,
                rootDir: user.path,
                needToRemember: req.body.needToRemember
            });
        } else {
            next(new WrongPasswordException(req.body.username));
        }
    }
});

router.post('/logout', (req, res, next) => {
    removeToken(req.body.access_token);
    res.status(200);
    res.json({
        data: "OK"
    });
});

module.exports = { users: router };
