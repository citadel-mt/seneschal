const webdav = require('webdav-server').v2;
const path = require('path');

const { rootDir } = require('../config');
const { users } = require(path.join(path.dirname(require.main.filename), "users.js"));
const { Basement } = require('../utils/Basement.js');
const { ControlManager } = require('../utils/ControlManager');

const userManager = new webdav.SimpleUserManager();
const webDavUsers = users.map((user) => {
    return {
        webDavUser: userManager.addUser(user.login, user.password, user.login === 'admin'),
        path: user.path,
        permissions: user.permissions
    };
});

const privilegeManager = new ControlManager();
webDavUsers.forEach((user) => {
    privilegeManager.setRights(user.webDavUser, user.path, user.permissions);
});

const webDavServer = new webdav.WebDAVServer({
    requireAuthentification: true,
    httpAuthentication: new webdav.HTTPBasicAuthentication(userManager),
    privilegeManager: privilegeManager
});

webDavServer.setFileSystem('/', new Basement(path.join(path.dirname(require.main.filename), rootDir)));

module.exports = { webDavServer };
