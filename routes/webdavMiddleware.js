const { findToken } = require("../utils/tokens");

const webdavMiddleware = () => async (req, res, next) => {
    if (req.headers.methodalias !== undefined) {
        req.method = req.headers.methodalias;
    }
    if (req.headers.authorization !== undefined) {
        const token = req.headers.authorization.split(" ")[1];
        const webDavToken = findToken(token);
        if (webDavToken !== undefined) {
            req.headers.authorization = `Basic ${webDavToken}`
        }
    }
    next();
};

module.exports = { webdavMiddleware };
