import React, { Component } from 'react';
import { axios } from 'webdav/dist/request'
import { connect } from 'react-redux';
import { getCookie, setCookie } from "./utils/cookies";

import Header from './components/Header';
import Navigation from './components/Navigation';
import Content from './components/content/Content';

import { setClient, updateUser, login, logout } from './actions/UserActions';
import { createDir, uploadFile, changePath, resetError } from "./actions/FilesActions";
import { showSideMenu, hideSideMenu,
         showNewDir, hideNewDir,
         showUploadFile, hideUploadFile } from "./actions/NavigationActions";
import Login from "./components/modals/Login";
import SideMenu from "./components/mobile/SideMenu";
import NewDirectory from "./components/modals/NewDirectory";
import UploadFile from "./components/modals/UploadFile";
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import Grid from "@material-ui/core/Grid";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#284147'
        },
        secondary: {
            main: '#432628'
        }
    },
    overrides: {
        MuiInput: {
            root: {
                '& label.Mui-focused': {
                    color: '#432628',
                },
                '& .MuiInput-underline:after': {
                    borderBottomColor: '#432628',
                },
            }
        },
        MuiTextField: {
            root: {
                '& label.Mui-focused': {
                    color: '#432628',
                },
                '& .MuiInput-underline:after': {
                    borderBottomColor: '#432628',
                },
            }
        }
    }
});

class App extends Component {

    constructor(props) {
        super(props);
        axios.interceptors.request.use(function (config) {
            if (config.method === "propfind") {
                config.method = "get";
                config.headers.methodalias = "propfind";
            } else if (config.method === "mkcol") {
                config.method = "post";
                config.headers.methodalias = "mkcol";
            }
            return config;
        }, function (error) {
            return Promise.reject(error);
        });
    }

    componentDidMount() {
        if (getCookie('access_token') !== undefined && getCookie('root_dir') !== undefined) {
            this.props.setClient(getCookie('access_token'), getCookie('root_dir'));
            this.props.updateUser({
                username: getCookie('username'),
                access_token: getCookie('access_token'),
                rootDir: getCookie("root_dir")
            });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.users.loggedUser !== undefined &&
            prevProps.users.loggedUser === undefined) {
                this.props.setClient(this.props.users.loggedUser.access_token,
                                     this.props.users.loggedUser.rootDir);
                if (this.props.users.loggedUser.needToRemember) {
                    setCookie('access_token', this.props.users.loggedUser.access_token);
                    setCookie('root_dir', this.props.users.loggedUser.rootDir);
                    setCookie('username', this.props.users.loggedUser.username);
                }
        }
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
                <SideMenu show={this.props.navigation.showSideMenu}
                          logout={this.props.logout}
                          loggedUser={this.props.users.loggedUser}
                          hideSideMenu={this.props.hideSideMenu}
                          showNewDir={this.props.showNewDir}
                          showUploadFile={this.props.showUploadFile} />
                <NewDirectory show={this.props.navigation.showNewDir}
                              webDavClient={this.props.users.webDavClient}
                              createDir={this.props.createDir}
                              changePath={this.props.changePath}
                              hideNewDir={this.props.hideNewDir}
                              path={this.props.content.path}
                              result={this.props.content.result}
                              error={this.props.content.error}
                              isFetching={this.props.content.isFetching}
                              resetError={this.props.resetError}/>
                <UploadFile show={this.props.navigation.showUploadFile}
                            webDavClient={this.props.users.webDavClient}
                            uploadFile={this.props.uploadFile}
                            changePath={this.props.changePath}
                            result={this.props.content.result}
                            error={this.props.content.error}
                            path={this.props.content.path}
                            hideUploadFile={this.props.hideUploadFile}
                            isFetching={this.props.content.isFetching}
                            resetError={this.props.resetError}/>

                {this.props.users.webDavClient === undefined &&
                    (<Login error={this.props.users.error}
                            login={this.props.login}
                            isFetching={this.props.users.isFetching}/>)}
                <Header isFetching={this.props.users.isFetching ||
                                    this.props.content.isFetching}
                        webDavClient={this.props.users.webDavClient}
                        loggedUser={this.props.users.loggedUser}
                        logout={this.props.logout}
                        changePath={this.props.changePath}
                        showSideMenu={this.props.showSideMenu}
                        showNewDir={this.props.showNewDir}
                        showUploadFile={this.props.showUploadFile} />
                <Grid container spacing={0} className="container-fluid" style={{
                    overflow: this.props.navigation.showSideMenu ? "hidden" : "auto"
                }}>
                    <Navigation />
                    <Content webDavClient={this.props.users.webDavClient}
                             content={this.props.content}
                             changePath={this.props.changePath} />
                </Grid>
            </ThemeProvider>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        content: store.content,
        users: store.users,
        navigation: store.navigation
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username, password, needToRemember) => dispatch(login(username, password, needToRemember)),
        logout: (access_token) => dispatch(logout(access_token)),
        setClient: (accessToken, path) => dispatch(setClient(accessToken, path)),
        updateUser: (props) => dispatch(updateUser(props)),

        changePath: (client, path) => dispatch(changePath(client, path)),
        createDir: (client, path) => dispatch(createDir(client, path)),
        uploadFile: (client, file, path) => dispatch(uploadFile(client, file, path)),
        resetError: () => dispatch(resetError()),

        showSideMenu: () => dispatch(showSideMenu()),
        hideSideMenu: () => dispatch(hideSideMenu()),
        showNewDir: () => dispatch(showNewDir()),
        hideNewDir: () => dispatch(hideNewDir()),
        showUploadFile: () => dispatch(showUploadFile()),
        hideUploadFile: () => dispatch(hideUploadFile())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
