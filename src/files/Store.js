import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import { rootReducer } from "./reducers";

export const store = createStore(rootReducer, applyMiddleware(promise, thunk, logger));
