import {
   CHANGE_PATH, CREATE_DIR,
   RESET_ERROR, SET_PATH,
   UPLOAD_FILE_REJECTED, UPLOAD_FILE_PENDING, UPLOAD_FILE_FULFILLED
} from "./ActionTypes";

export const createDir = (client, path) => ({
   type: CREATE_DIR,
   payload: client.createDirectory(path)
});

export const uploadFile = (client, file, path) => {
   const reader = new FileReader();
   return (dispatch) => {
      reader.onerror = (event) => {
         const error = event.target.error;
         dispatch({
            type: UPLOAD_FILE_REJECTED,
            payload: error
         });
      };
      reader.onload = (event) => {
         const result = event.target.result;
         client.putFileContents(path, result, {overwrite: false})
             .then((result) => dispatch({
                type: UPLOAD_FILE_FULFILLED,
                payload: result
             }))
             .catch((error) => dispatch({
                type: UPLOAD_FILE_REJECTED,
                payload: error
             }))
      };
      reader.readAsArrayBuffer(file);
      dispatch({
         type: UPLOAD_FILE_PENDING
      });
   }
};

export const saveFile = (client, resource) => ({

});

export const deleteResource = (client, resource) => ({

});

export const getStatInfo = (client, resource) => ({

});

export const renameResource = (client, resource) => ({

});




export const resetError = () => ({
   type: RESET_ERROR,
   payload: undefined
});

export const changePath = (client, path) => {
   return (dispatch) => {
      const response = dispatch({
         type: CHANGE_PATH,
         payload: client.getDirectoryContents(path)
      });

      response.then((content) => {
         dispatch(setPath(path))
      });
   }
};


//private

const setPath = (path) => ({
   type: SET_PATH,
   payload: path
});
