import {
   HIDE_NEW_DIR, HIDE_SIDE_MENU, HIDE_UPLOAD_FILE,
   SHOW_NEW_DIR, SHOW_SIDE_MENU, SHOW_UPLOAD_FILE
} from "./ActionTypes";

export const showSideMenu = () => ({
   type: SHOW_SIDE_MENU,
   payload: undefined
});

export const hideSideMenu = () => ({
   type: HIDE_SIDE_MENU,
   payload: undefined
});

export const showNewDir = () => ({
   type: SHOW_NEW_DIR,
   payload: undefined
});

export const hideNewDir = () => ({
   type: HIDE_NEW_DIR,
   payload: undefined
});

export const showUploadFile = () => ({
   type: SHOW_UPLOAD_FILE,
   payload: undefined
});

export const hideUploadFile = () => ({
   type: HIDE_UPLOAD_FILE,
   payload: undefined
});
