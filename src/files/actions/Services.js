import { apiURL } from '../config';

export function getAll(url) {
    return response(fetch(`${apiURL}${url}`));
}

export function get(url, index) {
    return response(fetch(`${apiURL}${url}/${index}`));
}

export function post(url, data) {
    console.log(`${apiURL}${url}`);
    return response(
        fetch(`${apiURL}${url}`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'content-type': 'application/json' },
        }),
    );
}

export function deleteOne(url, index) {
    return response(
        fetch(`${apiURL}${url}/${index}`, {
            method: 'DELETE',
        }),
    );
}

export function put(url, data, index) {
    return response(
        fetch(`${apiURL}${url}/${index}`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: { 'content-type': 'application/json' },
        }),
    );
}

function response(promise) {
    return promise
        .then(json)
        .then(status)
        .then((data) => data)
        .catch((err) => {
            throw err;
        });
}

function status(response) {
    if (!response.error_code) {
        return Promise.resolve(response);
    } else {
        return Promise.reject(response);
    }
}

function json(response) {
    return response.json();
}
