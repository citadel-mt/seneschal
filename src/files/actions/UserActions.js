import { createClient } from 'webdav';
import md5 from 'js-md5';
import { apiURL } from '../config';

import {SET_CLIENT, LOGIN, LOGOUT, UPDATE_USER} from "./ActionTypes";
import { post } from "./Services";

const webDavPath = "/webdav";

export const setClient = (accessToken, path) => ({
    type: SET_CLIENT,
    payload: createClient(
        `${apiURL}${webDavPath}${path}`,
        {
            token: {token_type: "Basic", access_token: accessToken}
        }
    )
});

export const updateUser = (props) => ({
   type: UPDATE_USER,
   payload: props
});

export const login = (username, password, needToRemember) => {
    return (dispatch) => {
        dispatch({
            type: LOGIN,
            payload: post('/users/login', {
                username: username,
                password: md5(password),
                needToRemember: needToRemember
            })
        });
    };
};

export const logout = (access_token) => ({
    type: LOGOUT,
    payload: post('/users/logout', {
        access_token: access_token
    })
});
