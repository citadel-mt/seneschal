import React, { Component } from 'react';
import './Header.scss';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ProfileButton from './ProfileButton';
import LinearProgress from "@material-ui/core/LinearProgress";

class Header extends Component {

    onBrandClick = () => {
        this.props.changePath(this.props.webDavClient, "/");
    };

    onLogoutClick = () => {
        this.props.logout(this.props.loggedUser.access_token);
    };

    showSideMenu = () => {
        this.props.showSideMenu();
    };

    showNewDir = () => {
        this.props.showNewDir();
    };

    showUploadFile = () => {
        this.props.showUploadFile();
    };

    render() {
        return (
            <AppBar position="fixed" className="appbar">
                <LinearProgress color="secondary" style={{
                    visibility: this.props.isFetching ? "visible" : "hidden"
                }}/>
                <Toolbar className="toolbar">
                    <Typography className="navbar-brand"
                                onClick={this.onBrandClick}
                                noWrap={true}
                                variant="h5">MT Storage</Typography>
                    <div id="navbar-content">
                        <div className="nav-container">
                            <Button className="nav-item"
                                    color="inherit"
                                    onClick={this.showUploadFile}>Upload file</Button>
                            <Button className="nav-item"
                                    color="inherit"
                                    onClick={this.showNewDir}>New directory</Button>
                        </div>
                        <ProfileButton name="header-profile-button"
                                       loggedUser={this.props.loggedUser}
                                       logout={this.props.logout}
                                       handleProfileClick={() => {}}/>
                        <IconButton className="hamburger"
                                    edge="end"
                                    color="inherit"
                                    aria-label="Menu"
                                    onClick={this.showSideMenu}>
                            <MenuIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;
