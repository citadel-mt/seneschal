import React, { Component } from 'react';
import './Navigation.scss';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

class Navigation extends Component {

    render() {
        return (
            <Grid item xs={2} className="left-nav">
                <TextField
                    id="standard-search"
                    color="inherit"
                    label="Search"
                    type="search"
                    margin="normal" />
            </Grid>
        );
    }
}

export default Navigation;
