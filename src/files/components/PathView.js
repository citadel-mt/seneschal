import React, { Component } from 'react';
import './PathView.scss';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

class PathView extends Component {

    onPathClick = (path) => {
        console.log(path);
        if (path === 'root') {
            this.props.changePath(this.props.webDavClient, "/");
        } else {
            const pathArr = this.props.path.split('/')
                .filter((p) => p.length !== 0);
            let pathTo = "/";
            for (let i=0; i<=pathArr.indexOf(path); i++) {
                pathTo = pathTo + pathArr[i] + "/";
            }
            this.props.changePath(this.props.webDavClient, pathTo);
        }
    };

    render() {
        const pathArr = this.props.path.split('/')
            .filter((p) => p.length !== 0);
        return (<Paper elevation={0} className="pathview">
                <Breadcrumbs separator="›" aria-label="Breadcrumb">
                    <Link className={`breadcrumb-item ${pathArr.length === 0 ? "active-item" : ""}`}
                          onClick={event => this.onPathClick("root")}>
                        root
                    </Link>
                    {pathArr.map((p, index) => (
                        <Link className={`breadcrumb-item ${pathArr.length-1 === index ? "active-item" : ""}`}
                              onClick={event => this.onPathClick(p)}>
                            {p}
                        </Link>
                    ))}
                </Breadcrumbs>
            </Paper>);
    }
}

export default PathView;
