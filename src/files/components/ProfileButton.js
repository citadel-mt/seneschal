import React, { Component } from 'react';
import Button from "@material-ui/core/Button";
import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import './ProfileButton.scss';

class ProfileButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profileMenuOpen: false
        }
    }

    handleToggle = () => {
        this.setState((state) => ({
            profileMenuOpen: !state.profileMenuOpen
        }));
    };

    handleClose = () => {
        this.setState((state) => ({
            profileMenuOpen: false
        }));
    };

    onLogoutClick = () => {
        this.props.handleProfileClick();
        this.props.logout(this.props.loggedUser.access_token);
    };

    render() {
        return (
            <div className="side-profile">
                <Button id={this.props.name}
                        ref={this.props.name}
                        onClick={this.handleToggle}>
                    {this.props.loggedUser === undefined ? "Guest" : this.props.loggedUser.username}
                    <KeyboardArrowDown />
                </Button>
                <Menu
                    id="lock-menu"
                    anchorEl={document.getElementById(this.props.name)}
                    keepMounted
                    open={this.state.profileMenuOpen}
                    onClose={this.handleClose} >
                    <MenuItem key="logout" onClick={this.onLogoutClick}>
                        Sign out
                    </MenuItem>
                </Menu>
            </div>
        );
    }
}

export default ProfileButton;
