import React, { Component } from 'react';
import './Content.scss';
import Grid from '@material-ui/core/Grid';
import ResourceRow from "./ResourceRow";
import windowDimensions from 'react-window-dimensions';
import Resource from "./res_types/Resource";
import PathView from "../PathView";

class Content extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.webDavClient !== undefined) {
            this.props.changePath(this.props.webDavClient, "/");
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.webDavClient === undefined && this.props.webDavClient !== undefined) {
            this.props.changePath(this.props.webDavClient, "/");
        }
    }

    render() {
        let viewWidth;
        let minMargin;
        if (document.body.clientWidth > 991) {
            viewWidth = document.body.clientWidth / 12 * 10 - 20;
            minMargin = Resource.minMargin;
        } else {
            viewWidth = document.body.clientWidth - 20;
            minMargin = Resource.minMobileMargin;
        }
        let numInRow;
        if (viewWidth - Resource.width < 0) {
            numInRow = 0;
        } else {
            numInRow = Math.floor((viewWidth - Resource.width) / (Resource.width + minMargin)) + 1;
        }
        const numOfRows = Math.ceil(this.props.content.fileTree.length / numInRow);
        const rows = [];
        for (let i=0; i<numOfRows; i++) {
            rows.push({
                from: i*numInRow,
                to: i*numInRow + numInRow
            });
        }
        const actualSpacing = (viewWidth - numInRow * Resource.width) / (numInRow - 1);
        return (
            <Grid item className="content" xs={document.body.clientWidth > 991 ? 10 : 12}>
                <PathView path={this.props.content.path}
                          webDavClient={this.props.webDavClient}
                          changePath={this.props.changePath} />
                {rows.map(row =>
                    (<ResourceRow resources={this.props.content.fileTree.slice(row.from, row.to)}
                                  path={this.props.content.path}
                                  webDavClient={this.props.webDavClient}
                                  changePath={this.props.changePath}
                                  spacing={actualSpacing} />)
                )}
            </Grid>
        );
    }
}

export default windowDimensions()(Content);
