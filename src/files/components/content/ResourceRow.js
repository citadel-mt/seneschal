import React, { Component } from 'react';
import Directory from "./res_types/Directory";
import File from "./res_types/File";
import './ResourceRow.scss';

const availableResType = {
    "directory": (props) => (<Directory {...props}/>),
    "file": (props) => (<File {...props}/>)
};

class ResourceRow extends Component {

    render() {
        return (<div className="resourceRow">
            {this.props.resources.map((r, index) => availableResType[r.type]({
                ...this.props,
                res: r,
                isFirst: index === 0
            }))}
        </div>);
    }
}

export default ResourceRow;
