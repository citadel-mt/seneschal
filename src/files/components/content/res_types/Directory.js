import React from 'react';
import Resource from "./Resource";

class Directory extends Resource {

    type = "folder-img";

    onClick = () => {
        console.log("Clicked: " + this.props.path);
        this.props.changePath(this.props.webDavClient, this.props.path+this.props.res.basename+"/");
    }
}

export default Directory;
