import React from 'react';
import Resource from "./Resource";

class File extends Resource {

    constructor(props) {
        super(props);
        this.type = File.chooseFileType(this.props.res.basename);
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        this.type = File.chooseFileType(nextProps.res.basename);
    }

    static chooseFileType = (basename) => {
        switch (basename.slice(basename.lastIndexOf('.')).toLowerCase()) {
            case ".txt":
                return "txt-img";
            case ".pdf":
                return "pdf-img";
            case ".js":
            case ".json":
                return  "js-img";
            case ".doc":
            case ".docx":
            case ".odt":
                return "doc-img";
            case ".xls":
            case ".xlsx":
            case ".ods":
                return "xls-img";
            case ".jpg":
            case ".jpeg":
                return "jpeg-img";
            case ".png":
                return "png-img";

            default:
                return "unknown-img";
        }
    }
}

export default File;
