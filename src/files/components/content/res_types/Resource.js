import React, { Component } from 'react';
import './Resource.scss';
import Typography from "@material-ui/core/Typography";

class Resource extends Component {

    static width = 86;
    static imageSize = 64;

    static minMargin = 40;
    static minMobileMargin = 20;

    type = "";

    onClick = () => {};

    render() {
        return (<div className={"resource"}
                     style={{
                         width: Resource.width + "px",
                         marginLeft: this.props.isFirst ? 0 : this.props.spacing + "px"
                     }}>
            <button className={this.type} style={{
                width: Resource.imageSize + "px",
                height: Resource.imageSize + "px",
                minWidth: Resource.imageSize + "px",
                minHeight: Resource.imageSize + "px",
            }} onClick={this.onClick}/>
            <Typography align="center" variant="body1">
                {this.props.res.basename}
            </Typography>
            {/*<p>{this.props.res.basename}</p>*/}
        </div>);
    }
}

export default Resource;
