import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import CloudUpload from '@material-ui/icons/CloudUpload';
import CreateNewFolder from '@material-ui/icons/CreateNewFolder';

import './SideMenu.scss';
import ProfileButton from "../ProfileButton";

class SideMenu extends Component {

    handleProfileClick = () => {
        this.props.hideSideMenu();
    };

    onUploadFileClick = () => {
        this.props.hideSideMenu();
        this.props.showUploadFile();
    };

    onNewDirClick = () => {
        this.props.hideSideMenu();
        this.props.showNewDir();
    };

    render() {
        return (
            <Drawer open={this.props.show} onClose={this.props.hideSideMenu} className="drawer">
                <div role="presentation" className="sidemenu" >
                    <List className="sidelist">
                        <div className="navheader">
                            <ProfileButton name="side-profile-button"
                                           loggedUser={this.props.loggedUser}
                                           logout={this.props.logout}
                                           handleProfileClick={this.handleProfileClick}/>
                        </div>
                        <div className="side-search">
                            <TextField
                                id="standard-search"
                                label="Search"
                                type="search"
                                margin="normal" />
                        </div>
                        <ListItem button key="Upload file" onClick={this.onUploadFileClick}>
                            <ListItemIcon><CloudUpload /></ListItemIcon>
                            <ListItemText primary="Upload file" />
                        </ListItem>
                        <ListItem button key="New directory" onClick={this.onNewDirClick}>
                            <ListItemIcon><CreateNewFolder /></ListItemIcon>
                            <ListItemText primary="New directory" />
                        </ListItem>
                    </List>
                </div>
            </Drawer>
        );
    }
}

export default SideMenu;
