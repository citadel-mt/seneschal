import React, { Component } from 'react';
import './Modal.scss';
import './Login.scss';
import Grid from "@material-ui/core/Grid";
import Checkbox from '@material-ui/core/Checkbox';
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            needToRemember: true
        }
    }

    onShowPassClick = () => {
        this.setState((state) => ({
            showPassword: !state.showPassword
        }));
    };

    onCheckedClick = () => {
        this.setState((state) => ({
            needToRemember: !state.needToRemember
        }));
    };

    onSubmit = () => {
        const username = document.getElementById("username").value;
        const password = document.getElementById("password").value;
        const needToRemember = this.state.needToRemember;
        this.props.login(username, password, needToRemember);
    };

    render() {
        const userNotExist = (this.props.error !== undefined &&
            this.props.error.error_message.startsWith("No such user"));
        const wrongPass = (this.props.error !== undefined &&
            this.props.error.error_message.startsWith("Wrong password"));
        return (<Grid container className="login-page">
            <Grid item className="login-back">
                <Paper elevation={5} className="login-form">
                    <div className="icon"/>
                    <Typography variant="h5" color="primary">
                        Welcome to MT Storage
                    </Typography>
                    <Typography variant="h6">
                        Sign in to continue
                    </Typography>

                    <Grid className="form-vertical" container direction="column" spacing={1}>
                        <Grid item>
                            <FormControl fullWidth={true} error={userNotExist}>
                                <InputLabel htmlFor="username">Login</InputLabel>
                                <Input id="username"
                                       aria-describedby="username_error"
                                       required={true}/>
                                <FormHelperText id="username_error" style={{
                                    visibility: userNotExist ? "visible" : "hidden"
                                }}>User does not exists</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl fullWidth={true} error={wrongPass}>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input id="password"
                                       type={this.state.showPassword ? 'text' : 'password'}
                                       aria-describedby="password_error"
                                       endAdornment={
                                       <InputAdornment position="end">
                                           <IconButton
                                               edge="end"
                                               aria-label="Toggle password visibility"
                                               onClick={this.onShowPassClick} >
                                               {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                           </IconButton>
                                       </InputAdornment>}/>
                                <FormHelperText id="password_error" style={{
                                    visibility: wrongPass ? "visible" : "hidden"
                                }}>Wrong password</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControlLabel
                                control={
                                    <Checkbox id="remember"
                                              color="primary"
                                              onChange={this.onCheckedClick}
                                              checked={this.state.needToRemember}/>
                                }
                                label="Remember me" />
                        </Grid>
                        <Grid item justify="center" className="but-container">
                            <Button variant="contained"
                                    color="primary"
                                    disabled={this.props.isFetching}
                                    onClick={this.onSubmit}>
                                Sign in</Button>
                            {this.props.isFetching && <CircularProgress size={24}
                                                                        className="but-progress"
                                                                        color="secondary"/>}
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>);
    }
}

export default Login;
