import React, { Component } from 'react';
import './Modal.scss';
import './FileArea.scss';
import Resource from '../content/res_types/Resource';
import Typography from "@material-ui/core/Typography";
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import Close from '@material-ui/icons/Close';
import IconButton from "@material-ui/core/IconButton";

class NewDirectory extends Component {

    onSubmit = () => {
        const dirname = document.getElementById("dirname").value;
        this.props.createDir(this.props.webDavClient, dirname);
    };

    onClose = () => {
        document.getElementById("dirname").value = "";
        this.props.resetError();
        this.props.hideNewDir();
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.show &&
            prevProps.result === undefined &&
            this.props.result !== undefined &&
            this.props.result.status === 201) {
                this.props.changePath(this.props.webDavClient, this.props.path);
                this.onClose();
        }
    }

    render() {
        return (<Modal
            className="mask"
            open={this.props.show}
            onClose={this.onClose} >
            <div className="modal-window" style={{
                height: "355px"
            }}>
                <div className="header">
                    <Typography variant="h5">
                        New directory
                    </Typography>
                    <IconButton onClick={this.onClose} className="close-button">
                        <Close fontSize="inherit"/>
                    </IconButton>
                </div>
                <div className="file-area">
                    <div className="resource">
                        <button className="folder-img" style={{
                            width: Resource.imageSize + "px",
                            height: Resource.imageSize + "px"
                        }}/>
                    </div>
                </div>
                <Grid className="form-vertical" direction="column" container spacing={2}>
                    <Grid item>
                        <FormControl fullWidth={true} error={this.props.error !== undefined}>
                            <InputLabel htmlFor="dirname">Directory name</InputLabel>
                            <Input id="dirname"
                                   aria-describedby="dirname_error"
                                   defaultValue={this.props.path+"New directory"}
                                   required={true}/>
                            <FormHelperText id="dirname_error" style={{
                                visibility: this.props.error ? "visible" : "hidden"
                            }}>Can't create directory</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item justify="center" className="but-container">
                        <Button variant="contained"
                                color="primary"
                                disabled={this.props.isFetching}
                                onClick={this.onSubmit}>
                            Create</Button>
                        {this.props.isFetching && <CircularProgress size={24}
                                                                    className="but-progress"
                                                                    color="secondary"/>}
                    </Grid>
                </Grid>
            </div>
        </Modal>)
    }
}

export default NewDirectory;
