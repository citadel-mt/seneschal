import React, { Component } from 'react';
import './Modal.scss';
import './FileArea.scss';
import Resource from '../content/res_types/Resource';
import File from '../content/res_types/File';
import Dropzone from 'react-dropzone';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import FormHelperText from "@material-ui/core/FormHelperText";
import Grid from "@material-ui/core/Grid";
import Close from "@material-ui/icons/Close";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from '@material-ui/core/IconButton';

class UploadFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedFile: undefined
        };
    }

    onFileDrop = (files) => {
        this.setState((state) => ({
            selectedFile: files[0]
        }));
    };

    onSubmit = () => {
        console.log(this.state.selectedFile);
        if (this.state.selectedFile !== undefined) {
            const uploadPath = document.getElementById("to_filename").value;
            this.props.uploadFile(this.props.webDavClient, this.state.selectedFile, uploadPath);
        }
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.show &&
            prevProps.result === undefined &&
            this.props.result !== undefined &&
            (this.props.result.status === 201 || this.props.result.status === 200)) {
                this.props.changePath(this.props.webDavClient, this.props.path);
                this.onClose();
        }
    }

    onClose = () => {
        this.setState(() => ({
            selectedFile: undefined
        }));
        document.getElementById("to_filename").value = "";
        this.props.resetError();
        this.props.hideUploadFile();
    };

    render() {
        return (<Modal
            className="mask"
            open={this.props.show}
            onClose={this.onClose} >
            <div className="modal-window" style={{
                height: "425px"
            }}>
                <div className="header">
                    <Typography variant="h5">
                        Upload file
                    </Typography>
                    <IconButton onClick={this.onClose} className="close-button">
                        <Close fontSize="inherit"/>
                    </IconButton>
                </div>
                <Dropzone onDrop={this.onFileDrop}>
                    {({getRootProps, getInputProps}) => (
                        <div className="file-area" {...getRootProps()}>
                            <div className="resource">
                                <div className={this.state.selectedFile === undefined ?
                                    "upload-img" :
                                    File.chooseFileType(this.state.selectedFile.name)}
                                     style={{
                                         width: Resource.imageSize + "px",
                                         height: Resource.imageSize + "px"
                                     }}/>
                                <input {...getInputProps()} />
                                <Typography align="center" variant="body1">
                                    {this.state.selectedFile === undefined ? "Choose file" : ""}
                                </Typography>
                            </div>
                        </div>
                    )}
                </Dropzone>
                <Grid className="form-vertical" container direction="column" spacing={2}>
                    <Grid item>
                        <FormControl fullWidth={true}>
                            <InputLabel htmlFor="from_filename">From</InputLabel>
                            <Input id="from_filename"
                                   value={this.state.selectedFile === undefined ? "" : this.state.selectedFile.path}
                                   readOnly={true}
                                   required={true}/>
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl fullWidth={true} error={this.props.error !== undefined}>
                            <InputLabel htmlFor="to_filename">To</InputLabel>
                            <Input id="to_filename"
                                   aria-describedby="to_filename_error"
                                   value={this.state.selectedFile === undefined ? "" : this.props.path+this.state.selectedFile.name}
                                   required={true}/>
                            <FormHelperText id="to_filename_error" style={{
                                visibility: this.props.error ? "visible" : "hidden"
                            }}>Can't upload file</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid item justify="center" className="but-container">
                        <Button variant="contained"
                                color="primary"
                                disabled={this.props.isFetching}
                                onClick={this.onSubmit}>
                            Upload</Button>
                        {this.props.isFetching && <CircularProgress size={24}
                                                                    className="but-progress"
                                                                    color="secondary"/>}
                    </Grid>
                </Grid>
            </div>
        </Modal>)
    }
}

export default UploadFile;
