import * as types from '../actions/ActionTypes';

const initState = {
    path: '/',
    fileTree: [],
    isFetching: false,
    result: undefined
};

export function contentReducer(state = initState, action) {
    switch (action.type) {
        case types.CHANGE_PATH_PENDING:
            return { ...state, isFetching: true };
        case types.CHANGE_PATH_FULFILLED:
            return { ...state, fileTree: action.payload, error: undefined, isFetching: false };
        case types.CHANGE_PATH_REJECTED:
            return { ...state, error: action.payload, isFetching: false };
        case types.SET_PATH:
            return { ...state, path: action.payload };

        case types.CREATE_DIR_PENDING:
            return { ...state, result: undefined, isFetching: true };
        case types.CREATE_DIR_FULFILLED:
            return { ...state, result: action.payload, error: undefined, isFetching: false };
        case types.CREATE_DIR_REJECTED:
            return { ...state, error: action.payload, isFetching: false };

        case types.UPLOAD_FILE_PENDING:
            return { ...state, result: undefined, isFetching: true };
        case types.UPLOAD_FILE_FULFILLED:
            return { ...state, result: action.payload, error: undefined, isFetching: false };
        case types.UPLOAD_FILE_REJECTED:
            return { ...state, error: action.payload, isFetching: false };

        case types.RESET_ERROR:
            return { ...state, error: undefined };

        default:
            return state;
    }
}
