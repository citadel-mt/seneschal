import { combineReducers } from 'redux';
import {contentReducer} from "./contentReducer";
import {usersReducer} from "./usersReducer";
import {navigationReducer} from "./navigationReducer";

export const rootReducer = combineReducers({
    content: contentReducer,
    users: usersReducer,
    navigation: navigationReducer,
});
