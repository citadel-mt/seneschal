import * as types from '../actions/ActionTypes';

const initState = {
    showSideMenu: false,
    showNewDir: false,
    showUploadFile: false
};

export function navigationReducer(state = initState, action) {
    switch (action.type) {
        case types.SHOW_SIDE_MENU:
            return { ...state, showSideMenu: true };
        case types.HIDE_SIDE_MENU:
            return { ...state, showSideMenu: false };

        case types.SHOW_NEW_DIR:
            return { ...state, showNewDir: true };
        case types.HIDE_NEW_DIR:
            return { ...state, showNewDir: false };

        case types.SHOW_UPLOAD_FILE:
            return { ...state, showUploadFile: true };
        case types.HIDE_UPLOAD_FILE:
            return { ...state, showUploadFile: false };
        default:
            return state;
    }
}
