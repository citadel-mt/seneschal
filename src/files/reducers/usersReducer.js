import * as types from '../actions/ActionTypes';
import { deleteCookie } from "../utils/cookies";

const initState = {
    loggedUser: undefined,
    webDavClient: undefined,
    isFetching: false
};

export function usersReducer(state = initState, action) {
    switch (action.type) {
        case types.SET_CLIENT:
            return { ...state, webDavClient: action.payload };
        case types.UPDATE_USER:
            return { ...state, loggedUser: { ...state.loggedUser, ...action.payload } };

        case types.LOGOUT_PENDING:
            return { ...state, isFetching: true };
        case types.LOGOUT_FULFILLED:
            deleteCookie('access_token');
            deleteCookie('root_dir');
            deleteCookie('username');
            return { ...state, loggedUser: undefined, webDavClient: undefined, error: undefined, isFetching: false };
        case types.LOGOUT_REJECTED:
            return { ...state, error: action.payload, isFetching: false };

        case types.LOGIN_PENDING:
            return { ...state, isFetching: true };
        case types.LOGIN_FULFILLED:
            return { ...state, loggedUser: action.payload, isFetching: false, error: undefined };
        case types.LOGIN_REJECTED:
            return { ...state, error: action.payload, isFetching: false };
        default:
            return state;
    }
}
