const
    defaultOptions = {
        path: '/',
        expires: 3600000 * 24 * 365
    };

export function getCookie(name) {
    const
        matches = document.cookie.match(new RegExp(
            '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
        ));

    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function setCookie(name, value, options = {}) {
    options = {
        ...defaultOptions,
        ...options
    };

    if (typeof options.expires === "number") {
        const d = new Date();

        d.setTime(d.getTime() + options.expires);
        options.expires = d;
    }

    /*
        Cookie expire time MUST be provided in the UTC string
    */
    if (options.expires && options.expires.toUTCString) {
        options.expires = options.expires.toUTCString();
    }

    value = encodeURIComponent(value);

    let updatedCookie = name + '=' + value;

    for (let propName in options) {
        updatedCookie += '; ' + propName;

        let propValue = options[propName];

        if (propValue !== true) {
            updatedCookie += '=' + propValue;
        }
    }

    document.cookie = updatedCookie;
}

export function deleteCookie(name) {
    setCookie(name, '', {
        expires: -86400000
    });
}
