const { PhysicalFileSystem } = require("webdav-server/lib/manager/v2/instances/PhysicalFileSystem");
const pathWorker = require('path');
const { rootDir, webDavPath } = require('../config');
const { renameFile } = require('./files');

class Basement extends PhysicalFileSystem {

    _move(pathFrom/* : Path*/,
          pathTo/* : Path*/,
          ctx/* : MoveInfo*/,
          callback/* : ReturnCallback<boolean>*/)/* : void*/ {
        if (pathFrom.paths[0] !== pathTo.paths[0]) {
            const pathFromStr = pathWorker.join(...pathFrom.paths);
            let pathToStr = pathWorker.join(...pathTo.paths);
            if (pathTo.paths[0] === webDavPath.slice(1)) {
                pathToStr = pathWorker.join(...pathTo.paths.slice(1));
            }
            console.log(pathFromStr);
            console.log(pathToStr);
            renameFile(pathWorker.join(pathWorker.dirname(require.main.filename), rootDir),
                       pathFromStr,
                       pathToStr);
        }
        callback();
    }
}

module.exports = { Basement };
