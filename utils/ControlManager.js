const { SimplePathPrivilegeManager } = require('webdav-server/lib/user/v2/privilege/SimplePathPrivilegeManager');
const { Path } = require("webdav-server/lib/manager/v2/Path");
const { webDavPath } = require('../config');

class ControlManager extends SimplePathPrivilegeManager {

    _can(fullPath, user, resource, privilege, callback) {
        if (fullPath.paths[0] === webDavPath.slice(1)) {
            super._can(new Path(fullPath.paths.slice(1)), user, resource, privilege, callback);
        } else {
            super._can(fullPath, user, resource, privilege, callback);
        }
    }
}

module.exports = { ControlManager };
