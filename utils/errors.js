class Exception extends Error {

    constructor(status, message) {
        super();
        this._status = status;
        this._message = message;
    }

    get status () {
        return this._status;
    }

    get message () {
        return this._message;
    }
}

class NoSuchUserException extends Exception {

    constructor(username) {
        super(400, `No such user ${username}`);
    }
}

class WrongPasswordException extends Exception {

    constructor(username) {
        super(400, `Wrong password for ${username}`);
    }
}

class FileSystemException extends Exception{

    constructor (file) {
        super(500, `Unknown file system exception: ${file}`);
    }
}

class NoSuchFileException extends Exception {

    constructor(file) {
        super(400, `No such file exception: ${file}`);
    }
}

class NoFileAccessException extends Exception {

    constructor (file) {
        super(403, `No file access: ${file}`);
    }
}

module.exports = { NoSuchUserException,
                   WrongPasswordException,
                   FileSystemException,
                   NoSuchFileException,
                   NoFileAccessException };
