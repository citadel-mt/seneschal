const fs = require('fs');
const path = require('path');
const { FileSystemException, NoSuchFileException, NoFileAccessException } = require('./errors');

async function getFileTree(path) {
    return await scanDir(path, path);
}

function scanDir(basePath, currentPath) {
    return new Promise((res, rej) => {
       fs.readdir(currentPath, (err, files) => {
           if (err) {
               rej(new FileSystemException(createRelativePath(currentPath, basePath, "")));
           } else {
               const result = [];
               const promises = [];
               files.forEach((file) => {
                   try {
                       const info = fs.statSync(path.join(currentPath, file));
                       if (info.isFile()) {
                           result.push({
                               name: createRelativePath(currentPath, basePath, file),
                               date_updated: info.mtime
                           });
                       } else if (info.isDirectory()){
                           promises.push(scanDir(basePath, path.join(currentPath, file)));
                       }
                   } catch (err) {
                       console.log(err);
                   }
               });
               Promise.all(promises)
                   .then(results => {
                       results.forEach((r) => {
                          result.push(...r);
                       });
                       res(result);
                   });
           }
       });
    });
}

function deleteFile(basePath, filePath) {
    const fullPath = path.join(basePath, filePath);
    return new Promise((res, rej) => {
        try {
            fs.accessSync(fullPath, fs.constants.F_OK);
        } catch (err) {
            rej(new NoSuchFileException(filePath));
        }
        try {
            fs.accessSync(fullPath, fs.constants.W_OK);
        } catch (err) {
            rej(new NoFileAccessException(filePath));
        }
        fs.unlink(fullPath, (err) => {
            if (err) {
                rej(new FileSystemException(filePath));
            } else {
                res(filePath);
            }
        });
    });
}

function createFile(basePath, filePath, fileContent) {
    const fullPath = path.join(basePath, filePath);
    const dirPath = path.parse(fullPath).dir;
    return new Promise((res, rej) => {
        try {
            fs.accessSync(dirPath, fs.constants.F_OK);
        } catch (err) {
            rej(new NoSuchFileException(dirPath))
        }
        try {
            fs.accessSync(dirPath, fs.constants.W_OK);
        } catch (err) {
            rej(new NoFileAccessException(dirPath));
        }
        fs.writeFile(fullPath, fileContent, (err) => {
            if (err) {
                rej(new FileSystemException(filePath));
            } else {
                res(filePath);
            }
        });
    });
}

function readFile(basePath, filePath) {
    const fullPath = path.join(basePath, filePath);
    return new Promise((res, rej) => {
        try {
            fs.accessSync(fullPath, fs.constants.F_OK);
        } catch (err) {
            rej(new NoSuchFileException(filePath))
        }
        try {
            fs.accessSync(fullPath, fs.constants.R_OK);
        } catch (err) {
            rej(new NoFileAccessException(filePath));
        }
        fs.readFile(fullPath, (err, data) => {
            if (err) {
                rej(new FileSystemException(filePath));
            } else {
                res(data);
            }
        });
    });
}

function createDir(basePath, dirPath) {
    const fullPath = path.join(basePath, dirPath);
    return new Promise((res, rej) => {
        fs.mkdir(fullPath, {recursive: true}, (err) => {
            if (err) {
                rej(new FileSystemException(dirPath));
            } else {
                res(dirPath);
            }
        });
    });
}

function renameFile(basePath, fromPath, toPath) {
    console.log(basePath);
    const fullPathFrom = path.join(basePath, fromPath);
    const fullPathTo = path.join(basePath, toPath);
    return new Promise((res, rej) => {
        try {
            fs.accessSync(fullPathFrom, fs.constants.F_OK);
        } catch (err) {
            rej(new NoSuchFileException(fromPath))
        }
        try {
            fs.accessSync(fullPathFrom, fs.constants.W_OK);
        } catch (err) {
            rej(new NoFileAccessException(fromPath));
        }
        fs.rename(fullPathFrom, fullPathTo, (err) => {
            if (err) {
                rej(new FileSystemException(fromPath));
            } else {
                res(toPath);
            }
        });
    });
}

function createRelativePath(currentPath, basePath, filePath) {
    return path.join(currentPath.replace(basePath, "."), filePath);
}

module.exports = { getFileTree, deleteFile, createFile, readFile, createDir, renameFile };
