const fs = require('fs');
const path = require('path');

function addToken(token, webDavToken) {
    fs.appendFileSync(path.join(path.dirname(require.main.filename),
        "tokens.properties"), `${token}=${webDavToken}\n`);
}

function removeToken(token) {
    let tokens = fs.readFileSync(path.join(path.dirname(require.main.filename),
        "tokens.properties")).toString().split("\n");
    tokens = tokens.filter((t) => t.split("=")[0] !== token);
    fs.writeFileSync(path.join(path.dirname(require.main.filename),
        "tokens.properties"), tokens.join("\n"));
}

function findToken(token) {
    const tokens = fs.readFileSync(path.join(path.dirname(require.main.filename),
        "tokens.properties")).toString().split("\n");
    const token_record = tokens.find((t) => t.split("=")[0] === token);
    if (token_record !== undefined) {
        return token_record.split("=")[1];
    }
}

module.exports = { addToken, removeToken, findToken };
